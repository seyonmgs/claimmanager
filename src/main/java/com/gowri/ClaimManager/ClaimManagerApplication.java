package com.gowri.ClaimManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class ClaimManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClaimManagerApplication.class, args);
	}
}
