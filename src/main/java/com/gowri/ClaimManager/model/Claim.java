package com.gowri.ClaimManager.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name="claims_master")
@Getter
@Setter
public class Claim extends Audit implements Serializable {
    @Id
    private int id;
    @NotNull
    private String claimID;
    @PositiveOrZero
    private BigDecimal amount;
}