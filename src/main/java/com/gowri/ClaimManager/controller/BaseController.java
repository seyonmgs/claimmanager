package com.gowri.ClaimManager.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BaseController {

    @GetMapping ("/claimsmanager")
    public String home() {
        return "Hello Claims Manager";
    }

}
